
var game = new Phaser.Game(1440, 800, Phaser.AUTO,'', { preload: preload, create: create, update: update, render: render  });

var earth;
var playerTank;
var cursors;
var currentSpeed=0;
var turret;
var shots;
var fireRate = 800;
var nextFire = 0;
var shot=true;
var shot1=false;
var shot2=false;
var shot3=false;
var simulate=true;
var playerHealthPoints=20;
var selectedAmmo = 'standard';
var shot1Available=false;
var shot2Available=false;
var shot3Available=false;
var explosions;
var enemies;
var enemyMisseles;
var enemiesCount = 20;
var enemiesAlive = 0;
var music;

Enemy = function (number, aMisseles) {
    var x = game.world.randomX;
    var y = game.world.randomY;
    this.healthPoints = 3;
    this.misseles = aMisseles;
    this.fireRate = 1000;
    this.nextFire = 0;
    this.alive = true;
    this.speed = 150;
    this.tank = game.add.sprite(x, y, 'tankbase');
    this.tankTurret = game.add.sprite(x, y, 'turret');
    this.tankTurret.animations.add('fire');
    this.tank.anchor.set(0.5,0.5);
    this.tankTurret.anchor.set(0.22, 0.49);
    this.tank.name = number.toString();
    game.physics.enable(this.tank, Phaser.Physics.ARCADE);
    this.tank.body.immovable = true;
    this.tank.body.collideWorldBounds = true;
    this.tank.body.bounce.setTo(1, 1);
    this.tank.angle = game.rnd.angle();
    this.tank.startAngl = this.tank.angle;
    this.tank.currentSpeed=this.speed;

};

Enemy.prototype.hit = function () {
    this.healthPoints--;
    if(this.healthPoints <= 0){
        this.alive = false;
        this.tank.kill();
        this.tankTurret.kill();
        return true;
    }
    return false;
};

Enemy.prototype.update = function () {

    this.tankTurret.x = this.tank.x;
    this.tankTurret.y = this.tank.y;


    if (game.physics.arcade.distanceBetween(this.tank, playerTank) < 500) {
        if (this.tank.rotation !== game.physics.arcade.angleBetween(this.tank, playerTank)) {
            if ((game.physics.arcade.angleBetween(this.tank, playerTank) - this.tank.rotation) < 1 || (game.physics.arcade.angleBetween(this.tank, playerTank) - this.tank.rotation > -1)) {
                this.tank.rotation = game.physics.arcade.angleBetween(this.tank, playerTank);
            } else {
                if (game.physics.arcade.angleBetween(this.tank, playerTank) - this.tank.rotation < -1)
                    this.tank.rotation += 0.01;
                else this.tank.rotation -= 0.01;
            }
        }
        this.tank.rotation = game.physics.arcade.moveToObject(this.tank, playerTank, this.tank.speed);
    }
    if (game.physics.arcade.distanceBetween(this.tank, playerTank) < 700)
    {
        if (this.tankTurret.rotation !== game.physics.arcade.angleBetween(this.tank, playerTank)) {
            if ((game.physics.arcade.angleBetween(this.tank, playerTank) - this.tankTurret.rotation < 1)||(game.physics.arcade.angleBetween(this.tank, playerTank) - this.tankTurret.rotation > -1)) {
                this.tankTurret.rotation = game.physics.arcade.angleBetween(this.tank, playerTank);
            } else{
                if(game.physics.arcade.angleBetween(this.tank, playerTank) - this.tankTurret.rotation < -1)
                    this.tankTurret.rotation += 0.01;
                else this.tankTurret.rotation -= 0.01;

            }
        }

    }
    if (game.physics.arcade.distanceBetween(this.tank, playerTank) < 500)

    this.tankTurret.rotation = game.physics.arcade.angleBetween(this.tank, playerTank);
    //game.physics.arcade.velocityFromRotation(this.tank.rotation, this.currentSpeed, this.tank.velocity);
    if (game.physics.arcade.distanceBetween(this.tank, playerTank) < 800)
    {
        if (this.tankTurret.rotation === game.physics.arcade.angleBetween(this.tank, playerTank)) {
            if (game.time.now > this.nextFire && this.misseles.countDead() > 0) {
                this.tankTurret.animations.play('fire', 20, false);
                this.nextFire = game.time.now + this.fireRate;
                var missele = this.misseles.getFirstDead();
                missele.reset(this.tankTurret.x, this.tankTurret.y);
                missele.rotation = game.physics.arcade.moveToObject(missele, playerTank, 500);
            }
        }

    }
};




function preload() {
    game.load.image('background','img/Dirt.png');
    // game.load.image('tankbase','img/tankbase.png');
    game.load.spritesheet('tankbase','img/basew.png',150,98,3);
    game.load.spritesheet('pltankbase','img/pltankBase.png',140,68);
    game.load.spritesheet('turretpl','img/turretapl.png',130,43,10);
    game.load.spritesheet('turret','img/turreta.png',180,54,14);
    game.load.image('shot','img/shot.png');
    game.load.image('shotsec','img/shot1.png');
    game.load.image('shotthi','img/shot2.png');
    game.load.image('shotfou','img/shot3.png');
    game.load.image('b1','img/bullet.png');
    game.load.image('b2','img/bullet1.png');
    game.load.image('b3','img/bullet2.png');
    game.load.image('b4','img/bullet3.png');
    game.load.audio('music', ['img/Czterej pancerni i pies-Deszcze nie spokojne.mp3','img/Czterej pancerni i pies-Deszcze nie spokojne.ogg']);
    game.load.spritesheet('explosion', 'img/explosion.png',64,64,23);
}

function create() {
    music = game.add.audio("music");

    music.play();

    game.world.setBounds(-1000, -1000, 5000, 5000);
    earth = game.add.tileSprite(0, 0, game.width, game.height, 'background');
    earth.fixedToCamera = true;

    playerTank = game.add.sprite(140,68,'pltankbase');//wstawienie podstawy czolgu

    game.physics.enable(playerTank, Phaser.Physics.ARCADE);
    playerTank.body.drag.set(0.2);
    playerTank.anchor.setTo(0.5,0.5);
    playerTank.body.maxVelocity.setTo(0, 0);
    playerTank.body.collideWorldBounds = true;

    enemyMisseles = game.add.group();
    enemyMisseles.enableBody = true;
    enemyMisseles.physicsBodyType = Phaser.Physics.ARCADE;
    enemyMisseles.createMultiple(100, 'shot', 0, false);

    enemyMisseles.setAll('anchor.x', 0.5);
    enemyMisseles.setAll('anchor.y', 0.5);
    enemyMisseles.setAll('outOfBoundsKill', true);
    enemyMisseles.setAll('checkWorldBounds', true);

    enemies = [];
    enemiesAlive = 20;

    for(var i = 0; i < enemiesCount; i++){
        enemies.push(new Enemy(i, enemyMisseles));
    }

    turret = game.add.sprite(0,0, 'turretpl');
    turret.anchor.setTo(0.20, 0.5);
    turret.animations.add('fire');

    explosions = game.add.group();

    shots = game.add.group();
    shots.enableBody = true;
    shots.physicsBodyType=Phaser.Physics.ARCADE;
    shots.createMultiple(30, 'shot', 0, false);
    shots.setAll('anchor.x', 0);
    shots.setAll('anchor.y', 0.5);

    playerTank.bringToTop();
    turret.bringToTop();
    //enemies.enableBody=true;
    enemies.physicsBodyType=Phaser.Physics.ARCADE;

    game.camera.follow(playerTank);
    game.camera.deadzone = new Phaser.Rectangle(400, 400, 400, 400);
    game.camera.focusOnXY(0, 0);

    cursors = game.input.keyboard.createCursorKeys();

    explosions.game.add.group();
    for (var i = 0; i < 10; i++)
    {
        var explosionAnimation = explosions.create(0, 0, 'explosion', [0], false);
        explosionAnimation.anchor.setTo(0.5, 0.5);
        explosionAnimation.animations.add('explosion');
    }


}


function update() {


    changefire();
    movetank();
    game.physics.arcade.overlap(enemyMisseles, playerTank, misseleHit, null, this);
    enemiesAlive = 0;
    for (var i = 0; i < enemies.length; i++)
    {
        if (enemies[i].alive)
        {
            enemiesAlive++;
            game.physics.arcade.collide(playerTank, enemies[i].tank);
            game.physics.arcade.overlap(shots, enemies[i].tank, misseleHitEnemy, null, this);
            enemies[i].update();
        }
    }

    if( playerHealthPoints <= 0 ){
        game.add.text(playerTank.x +150, playerTank.y, "Sory you lost\nClick F5 to restart",{
            fill: "#ff0000",
            align: "center"
        });
        game.paused = true;
    }
    if( enemiesAlive == 0){
        game.add.text(playerTank.x+150, playerTank.y, "You won\nNow you can practice your riding skills\nClick F5 to restart",{
            fill: "#00ff00",
            align: "center"
        });
    }


    game.physics.arcade.velocityFromRotation(playerTank.rotation, currentSpeed, playerTank.body.velocity);
    earth.tilePosition.x = -game.camera.x;
    earth.tilePosition.y = -game.camera.y;

    turret.rotation = game.physics.arcade.angleToPointer(turret);

    if(game.input.activePointer.isDown){
        fire();
    }
    if(game.input.keyboard.addKey(Phaser.Keyboard.P).isDown){
        if(simulate==true)
            simulate=false;
        else  simulate=true;
    }

}

function misseleHit(aTank, aShot) {
    aShot.kill();
    playerHealthPoints--;
}
function misseleHitEnemy(aTank, aShot){
    aShot.kill();
    if(enemies[aTank.name].hit()){
        if(!shot1Available){
            shot1Available = true;
        }
        else if(!shot2Available) {
            shot2Available = true;
        }
        else if(!shot3Available){
            shot3Available = true;
        }
        aTank.kill();

        var explosionAnimation = explosions.getFirstExists(false);
        explosionAnimation.reset(aTank.x, aTank.y);
        explosionAnimation.play('explosion', 30, false, true);
    }
}
function movetank() {
    if(simulate===false){
        if(currentSpeed===0){
            playerTank.animations.play('stop',40,true);
        }
        if (cursors.left.isDown || game.input.keyboard.addKey(Phaser.Keyboard.A).isDown)
        {
            playerTank.angle -= 1;
        }
        if (cursors.right.isDown || game.input.keyboard.addKey(Phaser.Keyboard.D).isDown)
        {
            playerTank.angle += 1;
        }

        if (cursors.up.isDown || game.input.keyboard.addKey(Phaser.Keyboard.W).isDown)
        {
            currentSpeed = 190;
        }
        if(cursors.down.isDown || game.input.keyboard.addKey(Phaser.Keyboard.S).isDown){

            currentSpeed = -100;
        }
        else
        {
            if (currentSpeed >= 1)
            {
                currentSpeed -= 3;
            }else if(currentSpeed === 1){
                currentSpeed =0;
            }
            if(currentSpeed<0){
                currentSpeed+=4;
            }
        }
    }
    else {
        if(currentSpeed===0){
            playerTank.animations.play('stop',40,true);
        }
        if (game.input.keyboard.addKey(Phaser.Keyboard.Q).isDown||game.input.keyboard.addKey(Phaser.Keyboard.D).isDown)
        {
            playerTank.angle += 1;
        }
        if (game.input.keyboard.addKey(Phaser.Keyboard.E).isDown||game.input.keyboard.addKey(Phaser.Keyboard.A).isDown)
        {
            playerTank.angle -= 1;
        }

        if (game.input.keyboard.addKey(Phaser.Keyboard.Q).isDown&&game.input.keyboard.addKey(Phaser.Keyboard.E).isDown)
        {
            currentSpeed = 190;
        }
        if(game.input.keyboard.addKey(Phaser.Keyboard.A).isDown&&game.input.keyboard.addKey(Phaser.Keyboard.D).isDown){

            currentSpeed = -100;
        }
        else
        {
            if (currentSpeed >= 1)
            {
                currentSpeed -= 3;
            }else if(currentSpeed === 1){
                currentSpeed =0;
            }
            if(currentSpeed<0){
                currentSpeed+=4;
            }
        }
    }
    turret.x = playerTank.x;
    turret.y = playerTank.y;
}
function fire() {
    if (game.time.now > nextFire && shots.countDead() > 0)
    {
        turret.animations.play('fire',20,false);

        nextFire = game.time.now + fireRate;

        var shot = shots.getFirstExists(false);

        shot.reset(turret.x, turret.y);

        shot.rotation = game.physics.arcade.moveToPointer(shot, 500, game.input.activePointer, 500);
    }
}
function changefire() {
    if(game.input.keyboard.addKey(Phaser.Keyboard.ONE).isDown){
        shot=true;
        shot1=false;
        shot2=false;
        shot3=false;
        if(shot===true){
            selectedAmmo = 'standard';
            shots = game.add.group();
            shots.enableBody = true;
            shots.physicsBodyType=Phaser.Physics.ARCADE;
            shots.createMultiple(30, 'shot', 0, false);
            shots.setAll('anchor.x', -7);
            shots.setAll('anchor.y', 0.5);
        }
    }
    if(game.input.keyboard.addKey(Phaser.Keyboard.TWO).isDown && shot1Available){
        shot=false;
        shot1=true;
        shot2=false;
        shot3=false;
        if(shot1===true){
            selectedAmmo = 'second';
            shots = game.add.group();
            shots.enableBody = true;
            shots.physicsBodyType=Phaser.Physics.ARCADE;
            shots.createMultiple(30, 'shotsec', 0, false);
            shots.setAll('anchor.x', -7);
            shots.setAll('anchor.y', 0.5);;
        }
    }
    if(game.input.keyboard.addKey(Phaser.Keyboard.THREE).isDown && shot2Available){
        shot=false;
        shot1=false;
        shot2=true;
        shot3=false;
        if(shot2===true){
            selectedAmmo = 'third';
            shots = game.add.group();
            shots.enableBody = true;
            shots.physicsBodyType=Phaser.Physics.ARCADE;
            shots.createMultiple(30, 'shotthi', 0, false);
            shots.setAll('anchor.x', -7);
            shots.setAll('anchor.y', 0.5);
        }
    }
    if(game.input.keyboard.addKey(Phaser.Keyboard.FOUR).isDown && shot3Available){
        shot=false;
        shot1=false;
        shot2=false;
        shot3=true;
        if(shot3===true){
            selectedAmmo = 'fourth';
            shots = game.add.group();
            shots.enableBody = true;
            shots.physicsBodyType=Phaser.Physics.ARCADE;
            shots.createMultiple(30, 'shotfou', 0, false);
            shots.setAll('anchor.x', -7);
            shots.setAll('anchor.y', 0.5);
        }
    }
    turret.bringToTop();
}
function render(){
    game.debug.text('Active ammo: ' + selectedAmmo, 32, 24,{
        fill: "#ff0000",
        align: "center"
    });
    game.debug.text('Player HP:   ' + playerHealthPoints*5 +'/ 100', 32, 44);
    var enemiesAliveDebug = 0;
    for (var i = 0; i < enemies.length; i++)
    {
        if (enemies[i].alive)
        {
            enemiesAliveDebug++;
        }
    }
    if(enemiesAliveDebug <= 16){
        shot1Available = true;
    }if(enemiesAliveDebug <= 10){
        shot2Available = true;
    }if(enemiesAliveDebug <= 5){
        shot3Available = true;
    }
    game.debug.text('Enemies alive:   ' + enemiesAliveDebug, 32, 64);
    game.debug.text('Simulation: ' + simulate, 32, 84);
}
